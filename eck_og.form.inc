<?php

/**
 * @file
 * ECK OG form processing functions.
 */

/**
 * Alter the "eck__bundle__edit_form" form adding OG specific settings.
 */
function eck_og_alter_bundle_edit_form(&$form, &$form_state) {
  $entity_type = $form['entity_type']['#value']->name;
  $entity_bundle = $form['bundle']['#value']->name;

  // Add the vertical_tabs, in case they are not defined.
  if (!isset($form['additional_settings'])) {
    $form['additional_settings'] = array(
      '#type' => 'vertical_tabs',
    );
  }

  // Move the submit above all form elements.
  $form['submit']['#weight'] = '999';

  // OG support.
  $form['og'] = array(
    '#type' => 'fieldset',
    '#title' => t('Organic groups'),
    '#collapsible' => TRUE,
    '#group' => 'additional_settings',
    '#description' => t('Specify how OG should treat content of this type. Content may behave as a group, as group content, or may not participate in OG at all.'),
  );

  // Group settings.
  $eck_bundle = get_bundle_admin_info($entity_type, $entity_bundle);
  $url = array('!url' => l(t('Manage fields'), $eck_bundle['path'] . '/fields'));
  $is_group = og_is_group_type($entity_type, $entity_bundle);

  $description = t('Set the content type to be a group, that content will be associated with, and will have group members.');
  if ($is_group) {
    $description .= '<br/>' . t('To unset the group definition you should delete the "Group type" field via !url.', $url);
  }

  $form['og']['og_group_type'] = array(
    '#type' => 'checkbox',
    '#title' => t('Group'),
    '#default_value' => $is_group,
    '#description' => $description,
    '#disabled' => $is_group,
  );

  // Group content settings.
  $is_group_content = og_is_group_content_type($entity_type, $entity_bundle);

  $description = t('Set the content type to be a group content, that can be associated with groups.');
  if ($is_group_content) {
    $description .= '<br/>' . t('To unset the group content definition or change the settings you should delete the "Groups audience" field via !url.', $url);
  }
  $group_content_options = og_get_all_group_entity();
  if (!$group_content_options) {
    $description .= '<br/>' . t('There are no group bundles defined.');
  }

  $form['og']['og_group_content_type'] = array(
    '#type' => 'checkbox',
    '#title' => t('Group content'),
    '#default_value' => $is_group_content,
    '#description' => $description,
    '#disabled' => !$group_content_options || $is_group_content,
  );

  // Hide the selection of the group content options.
  if ($group_content_options && !$is_group_content) {
    // Don't show the settings, as there might be multiple OG audience fields
    // in the same bundle.
    $form['og']['target_type'] = array(
      '#type' => 'select',
      '#title' => t('Target type'),
      '#options' => $group_content_options,
      '#default_value' => key($group_content_options),
      '#description' => t('The entity type that can be referenced thru this field.'),
      '#ajax' => array(
        'callback' => 'og_node_type_form_settings',
        'wrapper' => 'og-settings-wrapper',
      ),
      '#states' => array(
        'visible' => array(
          ':input[name="og_group_content_type"]' => array('checked' => TRUE),
        ),
      ),
      '#disabled' => $is_group_content,
    );

    $target_type = !empty($form_state['values']['target_type']) ? $form_state['values']['target_type'] : key($group_content_options);
    $entity_info = entity_get_info($target_type);
    $bundles = array();
    foreach ($entity_info['bundles'] as $bundle_name => $bundle_info) {
      if (og_is_group_type($target_type, $bundle_name)) {
        $bundles[$bundle_name] = $bundle_info['label'];
      }
    }

    // Get the bundles that are acting as group.
    $form['og']['target_bundles'] = array(
      '#prefix' => '<div id="og-settings-wrapper">',
      '#suffix' => '</div>',
      '#type' => 'select',
      '#title' => t('Target bundles'),
      '#options' => $bundles,
      '#default_value' => array(),
      '#size' => 6,
      '#multiple' => TRUE,
      '#description' => t('The bundles of the entity type that can be referenced. Optional, leave empty for all bundles.'),
      '#states' => array(
        'visible' => array(
          ':input[name="og_group_content_type"]' => array('checked' => TRUE),
        ),
      ),
      '#disabled' => $is_group_content,
    );
  }

  // Add our custom submit handler, that will attach fields to entity bundle.
  $form['#submit'][] = 'eck_og_bundle_edit_form_submit';
}

/**
 * Submit function for the eck bundle edit form.
 *
 * @see og_ui_node_type_save()
 *
 * @TODO: since we do not know how other custom entities are supporting OG, we
 * might need to restrict the target entity types.
 */
function eck_og_bundle_edit_form_submit($form, &$form_state) {
  $entity_type = $form['entity_type']['#value']->name;
  $bundle_name = $form['bundle']['#value']->name;
  $values = $form_state['values'];

  // Add the group field to this entity bundle.
  if ($values['og_group_type']) {
    og_create_field(OG_GROUP_FIELD, $entity_type, $bundle_name);
  }

  // Add the group content field to this entity bundle.
  if ($values['og_group_content_type'] && !og_is_group_content_type($entity_type, $bundle_name)) {
    // Let's copy the settings from the default og audience field.
    $field_name = OG_AUDIENCE_FIELD;
    $og_field = og_fields_info($field_name);

    // Entity reference is target entity specific, so we might need to create a
    // new field referencing our entity type.
    if ($values['target_type'] != 'node') {
      // Create a new field including the referenced entity in the field name.
      $field_name = 'og_group_' . $values['target_type'];
      $og_field['field']['field_name'] = $field_name;

      // @TODO: check if we need this for ECK.
      // Set our custom handler. This might change in the future, if we will
      // manage to work with the default OG handler.
      // $og_field['field']['settings']['handler'] = 'eck_og';.
      // In order to be more descriptive, add the target's entity label.
      $target_entity = entity_get_info($values['target_type']);
      $og_field['instance']['label'] = t('@entity Group audience', array('@entity' => $target_entity['label']));
    }

    // Set the target type and bundle.
    $og_field['field']['target_type'] = $values['target_type'];
    $og_field['field']['settings']['target_type'] = $values['target_type'];
    $og_field['field']['settings']['handler_settings']['target_bundles'] = $values['target_bundles'];

    og_create_field($field_name, $entity_type, $bundle_name, $og_field);
  }
}
